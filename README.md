My Crips (Ghost Theme) Config
=============================

This my clone of Kathy Qian's
[Crisp](https://github.com/kathyqian/crisp-ghost-theme) theme for
ghost.

I copied her repo and changed what she suggested and created my own
repo in order to try using it as a submodule.
